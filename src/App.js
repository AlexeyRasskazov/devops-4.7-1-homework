import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
	<h1>Test of revert</h1>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <p>Feature 1</p>
        <p>Feature 2</p>
        <p>Feature 3</p>
        <p>Feature 4</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
